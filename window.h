#ifndef WINDOW_H
#define WINDOW_H

#include <QSerialPort>
#include <QMainWindow>

class QDateTimeEdit;
class QLabel;
class QGroupBox;
class QLineEdit;

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window(QWidget *parent = nullptr);

protected:
    bool eventFilter(QObject *object, QEvent *event) override;

private slots:
    void slotCheckInMeasurement();
    void slotCheckOutMeasurement();
    void slotReadData();
    void slotPrintCheckin();
    void slotPrintCheckout();

private:

    const QString INPUT_PORT = "COM18";
    const QString OUTPUT_PORT = "COM2";
    const QString APP_NAME = "pesa v1.0";

    const QString LF = "\x0A";
    const QString HT = "\x09";
    const QString GS = "\x1D";
    const QString ESC = "\x1b";
    const QString InitializePrinter = ESC + "@";
    const QString RELEASE = GS + "V" + "\x42" + "\x0000";
    const QString BoldOn = ESC + '!' + "\x17";
    const QString BoldOff = ESC + '!' + "\x01";


    QString outputCheckin();
    QString outputCheckout();

    QDateTimeEdit *m_checkInDate;
    QDateTimeEdit *m_checkOutDate;
    QLineEdit *m_licensePlate;
    QLabel *m_weightCheckInLabel;
    QLabel *m_weightCheckOutLabel;
    QLabel *m_weightDifferenceLabel;
    QSerialPort m_inputPort;
    QSerialPort m_printerPort;
    QString m_inputBuffer;
};

#endif
