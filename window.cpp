#include <QComboBox>
#include <QDateTime>
#include <QDateTimeEdit>
#include <QDebug>
#include <QFormLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QKeyEvent>
#include <QMap>
#include <QPushButton>
#include <QRadioButton>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QStatusBar>
#include <QToolButton>
#include <QVBoxLayout>
#include "window.h"

Window::Window(QWidget *parent)
    : QMainWindow(parent)
{
    auto topWidget = new QWidget(this);
    QVBoxLayout *topLayout = new QVBoxLayout;
    QGroupBox *groupBoxCheckIn = new QGroupBox(tr("Check in"));
    // modificarlo perché va attivato solo con un click del pulsante "misura"
    QDateTime dateTimeCheckIn = QDateTime::currentDateTime();
    m_checkInDate = new QDateTimeEdit(dateTimeCheckIn);
    m_licensePlate = new QLineEdit();

    m_licensePlate->installEventFilter(this);

    m_weightCheckInLabel = new QLabel(this);
    m_weightCheckInLabel->installEventFilter(this);

    QPushButton *runMeasurementCheckIn = new QPushButton(tr("Esegui misurazione"));
    m_checkInDate->setReadOnly(true);
    m_checkInDate->setDateTime(dateTimeCheckIn);
    QFormLayout *fboxCheckIn = new QFormLayout;
    fboxCheckIn->addRow(tr("Targa:"), m_licensePlate);
    fboxCheckIn->addRow(tr("Data/ora entrata:"), m_checkInDate);
    fboxCheckIn->addRow(runMeasurementCheckIn);
    fboxCheckIn->addRow(tr("Peso in entrata:"), m_weightCheckInLabel);

    groupBoxCheckIn->setLayout(fboxCheckIn);


    QGroupBox *groupBoxCheckout = new QGroupBox(tr("Check out"));
    m_checkOutDate = new QDateTimeEdit(this);
    m_checkOutDate->setDateTime(dateTimeCheckIn);
    m_checkOutDate->setReadOnly(true);
    m_weightCheckOutLabel = new QLabel(this);
    m_weightCheckOutLabel->installEventFilter(this);
    m_weightDifferenceLabel = new QLabel(this);
    QPushButton *runMeasurementCheckOut = new QPushButton(tr("Esegui misurazione"));
    QFormLayout *fboxCheckOut = new QFormLayout;
    fboxCheckOut->addRow(tr("Data/ora uscita:"), m_checkOutDate);
    fboxCheckOut->addRow(runMeasurementCheckOut);
    fboxCheckOut->addRow(tr("Peso in uscita:"), m_weightCheckOutLabel);
    fboxCheckOut->addRow(tr("Peso netto:"), m_weightDifferenceLabel);
    groupBoxCheckout->setLayout(fboxCheckOut);

    topLayout->addWidget(groupBoxCheckIn);
    auto printButtonCin = new QPushButton("Stampa CheckIN", this);
    topLayout->addWidget(printButtonCin);
    topLayout->addWidget(groupBoxCheckout);
    connect(printButtonCin, &QPushButton::clicked, this, &Window::slotPrintCheckin);

    auto printButtonCout = new QPushButton("Stampa CheckOUT", this);
    topLayout->addWidget(printButtonCout);

    connect(printButtonCout, &QPushButton::clicked, this, &Window::slotPrintCheckout);

    topWidget->setLayout(topLayout);
    setCentralWidget(topWidget);

    setWindowTitle(APP_NAME);
    resize(480, 320);

    connect(runMeasurementCheckIn, &QPushButton::clicked, this, &Window::slotCheckInMeasurement);
    connect(runMeasurementCheckOut, &QPushButton::clicked, this, &Window::slotCheckOutMeasurement);

    connect(&m_inputPort, &QSerialPort::readyRead, this, &Window::slotReadData);

    m_inputPort.setPortName(INPUT_PORT);
    if (!m_inputPort.open(QIODevice::ReadOnly)) {
        qDebug() << "Impossibile aprire porta seriale in ingresso.";
        return;
    }

    m_inputPort.setBaudRate(QSerialPort::Baud9600);
    m_inputPort.setStopBits(QSerialPort::OneStop);
    m_inputPort.setDataBits(QSerialPort::Data8);
    m_inputPort.setParity(QSerialPort::NoParity);
    m_inputPort.setFlowControl(QSerialPort::NoFlowControl);

    m_printerPort.setPortName(OUTPUT_PORT);
    if (!m_printerPort.open(QIODevice::WriteOnly)) {
        qDebug() << "Impossibile aprire porta seriale per la stampante.";
        return;
    }

    m_printerPort.setBaudRate(QSerialPort::Baud9600);
    m_printerPort.setStopBits(QSerialPort::OneStop);
    m_printerPort.setDataBits(QSerialPort::Data8);
    m_printerPort.setParity(QSerialPort::NoParity);
    m_printerPort.setFlowControl(QSerialPort::NoFlowControl);
}

bool Window::eventFilter(QObject *object, QEvent *event)
{
    if (event->type() == QEvent::KeyRelease && object == m_licensePlate) {
        if (m_licensePlate->text().isEmpty()) {
            setWindowTitle(APP_NAME);
        } else {
            m_licensePlate->setText(m_licensePlate->text().toUpper());
            setWindowTitle(QStringLiteral("%1 - %2").arg(m_licensePlate->text(), APP_NAME));
        }
    }

    return false;
}

void Window::slotCheckInMeasurement()
{
    m_weightCheckInLabel->setText(m_inputBuffer);

    m_checkInDate->setDateTime(QDateTime::currentDateTime());

    const int checkInWeight = m_weightCheckInLabel->text().toInt();
    const int checkOutWeight = m_weightCheckOutLabel->text().toInt();
    m_weightDifferenceLabel->setText(QString::number(checkInWeight - checkOutWeight));
}

void Window::slotCheckOutMeasurement()
{
    m_checkOutDate->setDateTime(QDateTime::currentDateTime());
    m_weightCheckOutLabel->setText(m_inputBuffer);

    const int checkInWeight = m_weightCheckInLabel->text().toInt();
    const int checkOutWeight = m_weightCheckOutLabel->text().toInt();
    m_weightDifferenceLabel->setText(QString::number(checkInWeight - checkOutWeight));
}

void Window::slotReadData()
{
    m_inputBuffer = QString::fromLocal8Bit(m_inputPort.readAll());
    m_inputBuffer.remove("P");
    m_inputBuffer.remove("+");
    m_inputBuffer.remove("-");
    m_inputBuffer.remove(" ");
    m_inputBuffer.remove("@");
    //    |QRegularExpression("+")|QRegularExpression("-")|QRegularExpression(" "));
//    qDebug() << m_inputBuffer;
}

void Window::slotPrintCheckin()
{
    int bytes = m_printerPort.write(outputCheckin().toLocal8Bit());
    if (bytes == -1) {
        qDebug() << "Impossibile scrivere sulla porta seriale della stampante.";
    }

    qDebug() << "bytes scritti (CHECKIN):" << bytes;
}

void Window::slotPrintCheckout()
{
    int bytes = m_printerPort.write(outputCheckout().toLocal8Bit());
    if (bytes == -1) {
        qDebug() << "Impossibile scrivere sulla porta seriale della stampante.";
    }

    qDebug() << "bytes scritti (CHECKOUT):" << bytes;
}

QString Window::outputCheckin()
{
    const QDateTime inDateTime = m_checkInDate->dateTime();
    const QDateTime outDateTime = m_checkOutDate->dateTime();

    QString output = QStringLiteral("%1\n\n\n\n\n%2%3%4\n\n").arg(
        InitializePrinter,
        BoldOn+inDateTime.date().toString("dd/MM/yy"),
        HT+HT,
        inDateTime.time().toString("HH:mm")+BoldOff
    );

    output += QStringLiteral("TARGA%1PRIMA PESATA\n").arg(HT+HT);

    output += QStringLiteral("%1%2%3 KG\n\n%4").arg(
        BoldOn+m_licensePlate->text(),
        HT+HT,
        m_weightCheckInLabel->text()+BoldOff,
        ESC+RELEASE
    );

    qDebug() << Q_FUNC_INFO << output;
    return output;
}

QString Window::outputCheckout()
{
    const QDateTime inDateTime = m_checkInDate->dateTime();
    const QDateTime outDateTime = m_checkOutDate->dateTime();

    QString output = QStringLiteral("%1\n\n\n\n\n\n\n\n\n\n\n\n%2%3%4\n\n").arg(
        InitializePrinter,
        BoldOn+outDateTime.date().toString("dd/MM/yy"),
        HT+HT,
        outDateTime.time().toString("HH:mm")+BoldOff
    );

    output += QStringLiteral("%1SECONDA PESATA\n").arg(
        HT+HT
    );

    output += QStringLiteral("%1%2%3 KG\n\n").arg(
        HT,
        HT,
        BoldOn+m_weightCheckOutLabel->text()+BoldOff
    );

    output += QStringLiteral("%1%2PESO NETTO\n").arg(
        HT,
        HT
    );


    output += QStringLiteral("%1%2%3 KG\n%4").arg(
        HT,
        HT,
        BoldOn+m_weightDifferenceLabel->text()+BoldOff,
        ESC+RELEASE
    );

    qDebug() << Q_FUNC_INFO << output;
    return output;
}

